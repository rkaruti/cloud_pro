-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 29, 2023 at 04:40 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cloud`
--

-- --------------------------------------------------------

--
-- Table structure for table `attackerdetails`
--

CREATE TABLE `attackerdetails` (
  `id` int(10) NOT NULL,
  `filename` varchar(25) DEFAULT NULL,
  `hashkey` varchar(50) DEFAULT NULL,
  `server` varchar(30) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `attackerid` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attackerdetails`
--

INSERT INTO `attackerdetails` (`id`, `filename`, `hashkey`, `server`, `content`, `attackerid`) VALUES
(1, 'test.txt', '85549348', 'CS2', 'Hello Javaasdf', 'attacker');

-- --------------------------------------------------------

--
-- Table structure for table `dataowner`
--

CREATE TABLE `dataowner` (
  `uid` int(10) NOT NULL,
  `fname` varchar(50) DEFAULT NULL,
  `lname` varchar(50) DEFAULT NULL,
  `uname` varchar(50) DEFAULT NULL,
  `upass` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dataowner`
--

INSERT INTO `dataowner` (`uid`, `fname`, `lname`, `uname`, `upass`, `email`, `phone`) VALUES
(1, 'Owner', 'K', 'owner', 'owner', 'owner@gmail.com', '9653254741');

-- --------------------------------------------------------

--
-- Table structure for table `filedetails`
--

CREATE TABLE `filedetails` (
  `id` int(10) NOT NULL,
  `filename` varchar(25) DEFAULT NULL,
  `filekey` varchar(50) DEFAULT NULL,
  `hashkey` varchar(50) DEFAULT NULL,
  `userid` int(10) DEFAULT NULL,
  `server` varchar(30) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `auth` int(10) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `filedetails`
--

INSERT INTO `filedetails` (`id`, `filename`, `filekey`, `hashkey`, `userid`, `server`, `content`, `auth`) VALUES
(1, 'sample1.txt', '-1326363259', '-226219204', 1, 'CS1', 'V2VsY29tZSB0byBDbG91ZCBBcHBsaWNhdGlvbi4NCkJhbmdhbG9yZSwNCkthcm5hdGFrYSwNCkluZGlhLg0KDQpUaGlzIFBvcmplY3QgaXMgdG8gZGVtb3N0cmF0IGNsb3VkIHByb2plY3Q=', 1);

-- --------------------------------------------------------

--
-- Table structure for table `userdetails`
--

CREATE TABLE `userdetails` (
  `uid` int(10) NOT NULL,
  `fname` varchar(250) DEFAULT '0',
  `lname` varchar(250) DEFAULT '0',
  `uname` varchar(250) DEFAULT '0',
  `pass` varchar(250) DEFAULT '0',
  `email` varchar(250) DEFAULT '0',
  `dob` varchar(250) DEFAULT '0',
  `gender` varchar(250) DEFAULT '0',
  `phone` varchar(250) DEFAULT '0',
  `nationality` varchar(250) DEFAULT '0',
  `userstatus` varchar(25) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userdetails`
--

INSERT INTO `userdetails` (`uid`, `fname`, `lname`, `uname`, `pass`, `email`, `dob`, `gender`, `phone`, `nationality`, `userstatus`) VALUES
(1, 'Rose', 'Marry', 'rose', 'tiger', 'ramesh.karuti@yahoo.co.in', '2004-06-09', 'Female', '9898787676', 'Indian', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attackerdetails`
--
ALTER TABLE `attackerdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dataowner`
--
ALTER TABLE `dataowner`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `filedetails`
--
ALTER TABLE `filedetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userdetails`
--
ALTER TABLE `userdetails`
  ADD PRIMARY KEY (`uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attackerdetails`
--
ALTER TABLE `attackerdetails`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `dataowner`
--
ALTER TABLE `dataowner`
  MODIFY `uid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `filedetails`
--
ALTER TABLE `filedetails`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `userdetails`
--
ALTER TABLE `userdetails`
  MODIFY `uid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
