package com.megainfo.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.megainfo.dao.RegisterDAO;

/**
 * Servlet implementation class RegisterServlet
 */
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String userType = request.getParameter("usertype");
		RegisterDAO regisert = null;
		if (userType.equals("user")) {
			String firstname = request.getParameter("firstname");
			String lastname = request.getParameter("lastname");
			String username = request.getParameter("user");
			String password = request.getParameter("passwd");
			String email = request.getParameter("emailid");
			String dob = request.getParameter("dateof");
			String gender = request.getParameter("gender");
			String phone = request.getParameter("phone");
			String nationality = request.getParameter("nationality");

			if ((username != "") && (password != "")) {
				try {
					new RegisterDAO().registerUser(firstname, lastname, username, password, email, dob, gender, phone,
							nationality, nationality);
					request.setAttribute("msg", "Register Sucessfully..");
					RequestDispatcher rd = request.getRequestDispatcher("user.jsp");
					rd.forward(request, response);
				} catch (SQLException e) {
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				request.setAttribute("message", "Enter the valid details...");
				RequestDispatcher rd = request.getRequestDispatcher("userreg.jsp");
				rd.forward(request, response);
			}
		} else if (userType.equals("owner")) {
			
			String firstname = request.getParameter("firstname");
			String lastname = request.getParameter("lastname");
			String username = request.getParameter("user");
			String password = request.getParameter("passwd");
			String email = request.getParameter("emailid");
			String phone = request.getParameter("phone");

			if ((username != "") && (password != "")) {
				try {
					new RegisterDAO().registerOwner(firstname, lastname, username, password, email, phone);
					request.setAttribute("message", "Registration successful");
					RequestDispatcher rd = request.getRequestDispatcher("dataOwner.jsp");
					rd.forward(request, response);
				} catch (Exception e) {
					e.printStackTrace();
				}

			} else {
				request.setAttribute("message", "Registration Failed");
				RequestDispatcher rd = request.getRequestDispatcher("dataOwnerReg.jsp");
				rd.forward(request, response);
			}
		}
	}

}
