package com.megainfo.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.megainfo.dao.LoginDAO;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String userName = request.getParameter("username");
		String password = request.getParameter("password");
		String userType = request.getParameter("usertype");

		HttpSession session = request.getSession();

		LoginDAO login = new LoginDAO();

		if (userType.equals("user")) {
			int x = login.loginUser(userName, password);

			if ((userName != "") && (password != "")) {
				try {
					if (x != 0) {
						session.setAttribute("user", "user");
						session.setAttribute("userId", x);
						response.sendRedirect("userpage.jsp");
					} else {
						request.setAttribute("msg", "Wrong User Name/Password/User Blocked!");
						RequestDispatcher rd = request.getRequestDispatcher("user.jsp");
						rd.forward(request, response);
					}
				} catch (Exception e) {
					System.out.println("Exception " + e);
				}
			} else {
				request.setAttribute("msg", "Enter User Details!");
				RequestDispatcher rd = request.getRequestDispatcher("user.jsp");
				rd.forward(request, response);
			}
		} else if (userType.equals("owner")) {
			int x = login.loginOwner(userName, password);
			if ((userName != "") && (password != "")) {
				try {
					if (x != 0) {
						session.setAttribute("user", "dataowner");
						session.setAttribute("userId", x);
						session.setAttribute("username", userName);
						RequestDispatcher rd = request.getRequestDispatcher("dataOwnerpage.jsp");
						rd.forward(request, response);
					} else {
						request.setAttribute("message", "invalid username & password");
						RequestDispatcher rd = request.getRequestDispatcher("dataOwner.jsp");
						rd.forward(request, response);
					}
				} catch (Exception e) {
					System.out.println("Exception " + e);
				}
			}

			else {
				request.setAttribute("message", "Enter Details");
				RequestDispatcher rd = request.getRequestDispatcher("dataOwner.jsp");
				rd.forward(request, response);
			}
		}

		else if (userType.equals("cloud")) {
			System.out.println("Cloud Login");
			if ((userName != "") && (password != "")) {
				if ((userName.equals("cloud")) && (password.equals("cloud"))) {
					session.setAttribute("user", "cloud");
					response.sendRedirect("cloudServerPage.jsp");
				}
			} else {
				response.sendRedirect("cloudServer.jsp");
			}
		} else if (userType.equals("attacker")) {
			System.out.println("Attacker Login");
			if ((userName != "") && (password != "")) {
				if ((userName.equals("attacker")) && (password.equals("attacker"))) {
					session.setAttribute("user", "attacker");
					session.setAttribute("userId", userName);
					response.sendRedirect("attackerPage.jsp");
				} else {
					request.setAttribute("msg", "Invalid UserName/password");
					RequestDispatcher rd = request.getRequestDispatcher("attackerLogin.jsp");
					rd.forward(request, response);
				}
			} else {
				request.setAttribute("msg", "Enter Details");
				RequestDispatcher rd = request.getRequestDispatcher("attackerLogin.jsp");
				rd.forward(request, response);
			}
		} else if (userType.equals("tpa")) {
			System.out.println("Auditor Login");
			if ((userName != "") && (password != "")) {
				if ((userName.equals("tpa")) && (password.equals("tpa"))) {
					response.sendRedirect("authorityPage.jsp");
				} else {
					request.setAttribute("msg", "Checke User");
					RequestDispatcher rd = request.getRequestDispatcher("authority.jsp");
					rd.forward(request, response);
				}
			} else {
				request.setAttribute("msg", "Enter Details");
				RequestDispatcher rd = request.getRequestDispatcher("authority.jsp");
				rd.forward(request, response);
			}
		}
	}

}
