package com.megainfo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.megainfo.util.db.DBConnection;

public class LoginDAO {

	Connection con;
	PreparedStatement ps;
	ResultSet rs;

	public int loginUser(String username, String password) {
		System.out.println("Login User");

		int status = 0;
		String query = "Select uid, uname,pass from userdetails where uname='" + username + "' and pass='" + password
				+ "' and userstatus = '1'";
		try {
			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			if (rs.next()) {
				int id = rs.getInt(1);
				System.out.println("User " + id);
				status = id;
			} else {
				String queryUpdate = "UPDATE USERDETAILS SET USERSTATUS = '0' WHERE UNAME = '" + username + "'";
				ps = con.prepareStatement(queryUpdate);
				ps.executeUpdate();
			}
		} catch (Exception e) {
			System.out.println("Exception " + e);
		}
		return status;
	}
	
	public int loginOwner(String username, String password) {
		System.out.println("Login Owner");
		int status = 0;
		String query = "Select uid, uname, upass from dataowner where uname='" + username + "' and upass='"
				+ password + "'";
		try {
			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			if (rs.next()) {
				int id = rs.getInt(1);
				System.out.println("User " + id);
				status = id;
			} else {
				String queryUpdate = "UPDATE USERDETAILS SET USERSTATUS = '0' WHERE UNAME = '" + username + "'";
				ps = con.prepareStatement(queryUpdate);
				ps.executeUpdate();
			}
		} catch (Exception e) {
			System.out.println("Exception " + e);
		}
		return status;
	}

	public static void main(String[] args) {
		LoginDAO login = new LoginDAO();
		System.out.println("Status " + login.loginUser("rose", "tiger"));
	}

}
