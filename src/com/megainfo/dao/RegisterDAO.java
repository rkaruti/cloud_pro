package com.megainfo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.megainfo.util.db.DBConnection;

public class RegisterDAO {

	public int registerUser(String firstName, String lastName, String userName, String password, String email,
			String dob, String gender, String phone, String nationality, String userstatus) throws SQLException {

		Connection con = DBConnection.getDBConnection();
		PreparedStatement ps = con.prepareStatement(
				"insert into userdetails(fname, lname, uname, pass, email, dob, gender, phone, nationality, userstatus)"
						+ "values (?,?,?,?,?,?,?,?,?,?)");
		ps.setString(1, firstName);
		ps.setString(2, lastName);
		ps.setString(3, userName);
		ps.setString(4, password);
		ps.setString(5, email);
		ps.setString(6, dob);
		ps.setString(7, gender);
		ps.setString(8, phone);
		ps.setString(9, nationality);
		ps.setString(10, "0");
		int x = ps.executeUpdate();
		return x;
	}

	public int registerOwner(String firstName, String lastName, String userName, String password, String email,
			String phone) throws SQLException {

		Connection con = DBConnection.getDBConnection();
		PreparedStatement ps = con.prepareStatement(
				"insert into dataowner(fname, lname, uname, upass, email, phone) values (?, ?, ?, ?, ?, ?)");
		ps.setString(1, firstName);
		ps.setString(2, lastName);
		ps.setString(3, userName);
		ps.setString(4, password);
		ps.setString(5, email);
		ps.setString(6, phone);
		int x = ps.executeUpdate();
		return x;
	}

}
